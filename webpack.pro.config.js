'use strict'
const config = require('./config/index.js');
const merge = require('webpack-merge');
const baseWebpackConfig = require('./webpack.config.js');

const proWebpackConfig = merge(baseWebpackConfig, {
  plugins: config.build.plugins
});

module.exports = proWebpackConfig;