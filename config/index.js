'use strict'

const webpack = require("webpack");

module.exports = {
  dev: {
    plugins: [
      new webpack.HotModuleReplacementPlugin(),
      new webpack.optimize.CommonsChunkPlugin({
        name: ['common'],
        //chunks: ['index', 'detail']
        minChunks: 2
      }),
      new webpack.optimize.UglifyJsPlugin({
        compress: {
          warnings: false
        }
      })
    ]
  },
  build: {
    plugins: [
      new webpack.optimize.CommonsChunkPlugin({
        name: ['common'],
        //chunks: ['index', 'detail']
        minChunks: 2
      }),
      new webpack.optimize.UglifyJsPlugin({
        compress: {
          warnings: false
        }
      })
    ]
  }
}
