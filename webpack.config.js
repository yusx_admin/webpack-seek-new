const path = require("path");
const webpack = require("webpack");
const htmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const extractCss = new ExtractTextPlugin('css/[name].css');
const OptimizeCSSPlugin = require('optimize-css-assets-webpack-plugin');

module.exports = {
  devtool: 'eval',
  entry: {
    index: ['./src/js/index.js', './src/js/common.js'],
  },
  output: {
    path: path.resolve(__dirname, 'app'),
    filename: 'js/[name].js',
    publicPath: './'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        include: path.resolve(__dirname, 'src/js'),
        exclude: path.resolve(__dirname, 'node_modules')
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: 'style-loader'
          },
          {
            loader: 'css-loader',
            options: {
              importLoaders: 1
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              config: {
                path: './src/config/postcss.config.js'
              }
            }
          }
        ]
      },
      {
        test: /\.styl(us)?$/,
        use: extractCss.extract({
          publicPath: '../',//用于提取css文件后，修正css中加载的图片路径不正确的问题
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader',
              options: {
                importLoaders: 1
              }
            },
            {
              loader: 'postcss-loader',
              options: {
                config: {
                  path: './config/postcss.config.js'
                }
              }
            },
            'stylus-loader'
          ]
        })
      },
      {
        test: /\.html$/,
        loader: ['html-loader']
      },
      {
        test: /\.tpl$/,
        use: ['ejs-loader']
      },
      {
        test: /\.(jpg|png|gif)$/i,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 10000,
              name: './img/[name]-[hash:5].[ext]'
              //貌似只处理css中引入的图片
              //如果要处理html中的图片，建议将图片保存在编译目录（app目录）
            }
          }
        ]
      },
      {
        test: /\.(woff|woff2|svg|eot|ttf)\??.*$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              name: 'fonts/[name].[ext]',
              limit: 10000
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new htmlWebpackPlugin({
      title: '订单详情',
      filename: 'index.html',
      template: path.resolve(__dirname, 'src/components/tpl-index.ejs'),
      inject: 'body',
      chunks: ['index', 'common'],
      minify: {
        //removeComments: true,
        //collapseWhitespace: true,
      }
    }),
    new OptimizeCSSPlugin({
      cssProcessor: require('cssnano'),
      cssProcessorOptions: {
        discardComments: {removeAll: true},
        safe: true
      },
      canPrint: false
    }),
    extractCss
  ]
}
