const config = require('./config/index.js');
const path = require('path');
const merge = require('webpack-merge');
const baseWebpackConfig = require('./webpack.config.js');

const devWebpackConfig = merge(baseWebpackConfig, {
  devServer: {
    contentBase: path.resolve(__dirname, 'app'),
    compress: true,
    port: 9000,
    inline: true,
    open: true,
    publicPath: '/'
  },
  plugins: config.dev.plugins
});

module.exports = devWebpackConfig;